## Описание
Будет позже...

## Запуск проекта в dev-режиме на локальном сервере:
Клонировать репозиторий и перейти в директорию с ним:

```bash
git clone git@gitlab.com:boostnet/label-set.git
```
```bash
cd label-set
```

Установить пакеты
```bash
yarn install
cd server && yarn install && cd ..
cd client && yarn install && cd ..
```

Сервер выполнит миграции при первом запуске, если файл базы данных еще не существует.

Запуск в dev режиме:

```bash
env PORT=3000 API_PORT=3001 yarn start
```

## Сборка в продакт

Сборка клиентского приложения:

```bash
cd client && yarn run build && cd ..
```

Запуск сервера в продакт:

```bash
env PORT=80 NODE_ENV=production node server/src/index.js
```

## Параметры


- `PORT` - Порт
- `API_PORT` - Порт API (только для dev режима)
- `UPLOADS_PATH` - абсолютный путь до директории с изображениями
- `DATABASE_FILE_PATH` - абсолютный путь до файла базы. По умолчанию `database.sqlite` находится в директории server.
- `ADMIN_PASSWORD` - админ пароль (установка меток пользователем, без авторизации, нужен только в админ часть)


